/*
 * @author Qingqing Wu, 991500423
 */

package password;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {
	
	// Lab5 exercise with branching and merging
	@Test
	public void testHasValidCaseCharsRegular() {
		// password must contains a CAP, a Lower case, and not be null
		boolean result = PasswordValidator.hasValidCaseChars("aAaAaaa11");
		assertTrue("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryIn() {
		boolean result = PasswordValidator.hasValidCaseChars("aA");
		assertTrue("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionBlank() {
		boolean result = PasswordValidator.hasValidCaseChars("");
		assertFalse("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNull() {
		boolean result = PasswordValidator.hasValidCaseChars(null);
		assertFalse("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNumber() {
		boolean result = PasswordValidator.hasValidCaseChars("223455");
		assertFalse("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper() {
		boolean result = PasswordValidator.hasValidCaseChars("AAAA");
		assertFalse("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutLower() {
		boolean result = PasswordValidator.hasValidCaseChars("aaaa");
		assertFalse("Invalid case characters", result);
	}
	

	
	
	
	
	
	
	// Lab3
	@Test
	public void testIsValidLength() {
		assertTrue("Invalid password length",PasswordValidator.isValidLength("1234567890"));
	}
	
	@Test
	public void testIsValidLengthException() {
		assertFalse("Invalid password length",PasswordValidator.isValidLength(null));
	}
	
	@Test
	public void testIsValidLengthExceptionSpaces() {
		assertFalse("Invalid password length",PasswordValidator.isValidLength("          "));
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		assertTrue("Invalid password length",PasswordValidator.isValidLength("12345678"));
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		assertFalse("Invalid password length",PasswordValidator.isValidLength("1234567"));
	}
	
	@Test
	public void testHasEnoughDigits() {
		assertTrue("Invalid password characters", PasswordValidator.hasEnoughDigits("12dddddd"));
	}

	@Test
	public void testHasEnoughDigitsException() {
		assertFalse("Invalid password characters", PasswordValidator.hasEnoughDigits(null));
	}
	
	@Test
	public void testHasEnoughDigitsBoundaryIn() {
		assertTrue("Invalid password characters", PasswordValidator.hasEnoughDigits("12aaaaaa"));
	}
	
	@Test
	public void testHasEnoughDigitsBoundaryOut() {
		assertFalse("Invalid password characters", PasswordValidator.hasEnoughDigits("1aaaaaaa"));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
